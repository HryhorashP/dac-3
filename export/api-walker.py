#!/usr/bin/env python

import argparse
import json
import logging as log
import re

import requests


def get_options():
    parser = argparse.ArgumentParser(
        description='scraps the script from solutions')
    parser.add_argument('target',
                        metavar='target',
                        help='the target root page to extract navigation from')
    return parser.parse_args()


def collect_page_and_children(page):
    data = json.loads(requests.get(page).text)
    parent = json.loads(requests.get('%s/child/page' % page).text)
    children = []
    if 'results' in parent.keys():
        for r in parent['results']:
            children.append({'id': r['id'], 'title': r['title']})
    return {'title': data['title'], 'id': data['id'], 'children': children}


def collect_flat_page_tree(page, current_depth, depth=3, result={'rank': 0}):
    current_depth += 1
    log.info('depth: %s', current_depth)
    if current_depth > depth:
        log.info('returning: %s', current_depth)
        return
    parent_and_children = collect_page_and_children(page)
    result['rank'] += 1
    parent_and_children['rank'] = current_depth * 100 + result['rank']
    result[parent_and_children['id']] = parent_and_children
    if len(parent_and_children['children']) > 0:
        print((current_depth - 1) * '  ', '- pages:')
    for r in parent_and_children['children']:
        print(current_depth * '  ', '- id:', r['id'])
        print(current_depth * '  ', '  title:', r['title'])
        print(current_depth * '  ', '  slug:',
              '%s-%s.html' % (re.sub('\W+', '-', r['title']).lower(), r['id']))
        next_level = 'https://developer.atlassian.com/rest/api/content/%s' % r['id']
        collect_flat_page_tree(next_level, current_depth, result=result)
    return result


if __name__ == '__main__':
    options = get_options()
    # root_api_page='https://confluence.atlassian.com/rest/api/content/764477791'
    # https://developer.atlassian.com/rest/api/content/39375886
    root_api_page = options.target
    print("---")
    print("\"Hierarchy\":")
    flat = collect_flat_page_tree(root_api_page, 0, depth=4)
    #from IPython import embed
    #embed()
