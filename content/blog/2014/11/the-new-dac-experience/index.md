---
title: "A New Developer Experience"
date: "2014-11-18T14:00:00+07:00"
author: "nwade"
categories: ["developer experience"]
---

Our developer relations team has recently been hard at work building a better 
[developer.atlassian.com](https://developer.atlassian.com/). Today, we're excited to launch 
a whole new experience; a new landing page, a fresh new view of our content in Confluence, and our new [developer blog](/blog). 

###Data-driven redesign

We went to our data to see how developers and readers were actually using our content. 
We looked at trends in search terms, page hits, and user journeys on pages; and created a new navigation structure to 
support the destinations we found to be important to you. We organized our information architecture and landing
page around these analytics, and we hope it helps you find resources faster. To explore, click **Menu** in the upper
right corner. You should see a way to navigate to essential product developer documentation. 
If you ever end up in the wrong place, you can easily navigate backwards. And we're rolling out 
this new navigation to all our developer documentation spaces. 

We've also applied a fresh new look and feel to the site to align with our 
[design guidelines](https://design.atlassian.com/). You'll notice updated typography, clean 
backgrounds, and an overall refresh for most of our content that's still hosted in our good friend [Confluence](https://www.atlassian.com).

<img src="dac-redesign.png" alt="New homepage" style="width:80%;border:1px solid black;display: block;
margin-right:auto;margin-left:auto;margin-top:30px;margin-bottom:30px;">  


###Developer blog  

We're also pleased to announce our developer blog, you're reading this post on it now of course
(the index is [/blog](/blog)). This blog is dedicated to our technical community. We'll be sharing interesting
things our engineers are doing; new details about Atlassian Connect, product APIs, and product releases;
cool or notable technology updates like Git releases and new workflows; and even guest blogs from other
developers with interesting technology discussions to share. For example, expect more posts soon about how we
built the new [developer.atlassian.com](https://developer.atlassian.com/) site; how we handle this blog; and our
new [Hello World experience](https://try.atlassianconnect.com) which is entirely in the browser in two minutes.

###Ways to engage

Like any useful blog we've built a feed for you to simply add to your reader of choice - 
[Atlassian Developer blog feed](https://developer.atlassian.com/blog/feed.xml). We've included feedback
reporting, social sharing buttons, and the same comments system at the foot of every post and every page on the site,
and we'd love for you to engage with these whenever you feel inclined. And of course, if you have any specific
questions for us or for your fellow developers you can always talk to us and other developers
in [our forums](/help#community).

Thanks for engaging with our new developer site. Again, feel free to leave your feedback or requests below or via [our help page](/help#contact-us)!
