---
title: "Node Summit 2015 videos for Atlassian developers"
date: "2015-03-20T17:00:00+07:00"
author: "rwhitbeck"
categories: ["Nodejs", "Javascript", "Microservices", "Add-ons"]
---
<style type="text/css">
.img {
	border: 0;
	margin: 20px auto;
	text-align: center;
}
</style>
Last month, Atlassian was a proud [supporter](http://nodesummit.com/sponsors/#sponsors-silver)
 of the [Node Summit](http://nodesummit.com/) in San Francisco.  We were there to talk to 
 developers about using NodeJS to build add-on microservices for Atlassian Cloud products. 
 Earlier this week, Node Summit released the [videos from the 2015 Conference](http://nodesummit.com/media/category/sf-2015/).  
 Building an add-on with Node? Here are a few useful sessions!


## Why Would We Ever Build a Distributed Computing Platform in Node?

<div class="img">
	<a href="http://nodesummit.com/media/why-would-we-ever-build-a-distributed-computing-platform-in-node/">
		<img src="video1screen.jpg" alt="Video of Why Would We Ever Build a Distributed Computing Platform in Node">
	</a>
</div>

Gord Tanner of bitHound talks about how they developed a large scale distributed computing 
platform to analyze source code in Node.js.  Find out what worked and didn't work in 
architecting the back end services of bitHound.

[Watch (28:28)](http://nodesummit.com/media/why-would-we-ever-build-a-distributed-computing-platform-in-node/)

<hr>

## Deploy your Application to Production Today

<div class="img">
	<a href="http://nodesummit.com/media/deploy-your-application-to-production-today/">
		<img src="video2screen.jpg" alt="Video of Deploy your Application to Production Today">
	</a>
</div>

Brandon Cannaday and Taron Foxworth of Modulus talks about how simple it is to take your 
Node.js application and provide it to all the world to enjoy.

[Watch (10:19)](http://nodesummit.com/media/deploy-your-application-to-production-today/)

<hr>

## Node.js at Scale

<div class="img">
	<a href="http://nodesummit.com/media/node-js-at-scale/">
		<img src="video3screen.jpg" alt="Video of Node.js at Scale">
	</a>
</div>

CJ Silverio from npm discusses the npm registry and the signs of strain it was showing after 
4 years of being a side project and suddenly finding itself in massive growth. By the end of 
2014 it became a stable critical component to the Node.js experience. Look at the path to get 
there and learn about the challenges involved to refactor critical infrastructure without downtime. 

[Watch (25:07)](http://nodesummit.com/media/node-js-at-scale/)

<hr>

## Measuring Node.js Microservices

<div class="img">
	<a href="http://nodesummit.com/media/measuring-node-js-micro-services/">
		<img src="video4screen.jpg" alt="Video of Measuring Node.js Microservices">
	</a>
</div>

Peter Elger of nearForm discusses embracing and build microservices without fear of creating a monster!

[Watch (28:11)](http://nodesummit.com/media/measuring-node-js-micro-services/)

<hr>

## Node.js and Containers: Dispatches from the Frontier

<div class="img">
	<a href="http://nodesummit.com/media/node-js-and-containers-dispatches-from-the-frontier/">
		<img src="video5screen.jpg" alt="Video of Node.js and Containers: Dispatches from the Frontier">
	</a>
</div>

Bryan Cantrill of Joyent talks about the explosion of developer interest in containers which 
are a good match for efficiently developing and deploying microservices architectures. Bryan 
shares his experiences using Node.js containers to build distributed systems, including the 
difficulties, improvements and successes.  Additionally, explore the challenges that remain. 

[Watch (36:29)](http://nodesummit.com/media/node-js-and-containers-dispatches-from-the-frontier/)

<hr>

What sessions from Node Summit did you find useful? Let us know in the comments or tweet at us at [@atlassiandev](http://twitter.com/atlassiandev) or directly to me at [@RedWolves](http://twitter.com/redwolves).




