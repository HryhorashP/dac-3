---
title: "Atlassian and open source - developer tools engineering"
date: "2015-09-23T16:00:00+07:00"
author: "mmelnicki"
categories: ["opensource", "headsof"]
---

This post is part of a recurring series from the Heads of Engineering at Atlassian.  This week, we're featuring 
Mike Melnicki.  He's talking to us about how the developer tools engineering team interacts with the world of open source.

## Tell us a little about your group at Atlassian
We're in the middle of a major shift in how software is developed across the globe. The primary driver of this shift is the 
need for more software at a faster rate and by more organizations than ever before. These shifts not only are changing how 
teams build software but also the tools they use to do so. We in the developer tools engineering team strive to bring modern 
software development to all teams. We are spread across Sydney, San Francisco, Austin, and Gdańsk and we collectively work on 
Bitbucket, Bamboo, FishEye, Crucible, SourceTree, and Clover. One of the amazing things about the dev tools engineering team 
is that we get to use our own products every day through the normal course of our work. As users of our own products, we're 
very passionate about making our tools as good as they can be because they help us unlock our maximum potential as a team.

## How does your team benefit from open source software?
Our team benefits from the open source community immensely. Git and Mercurial are probably the most obvious projects I should 
mention because they are the heart of our developer tool products. Almost all of our cloud infrastructure is built upon open source 
projects such as PostgresSQL, Redis, Memcached, OpenSSL, Nginx, HAProxy, RabbitMQ, Tomcat, and many others. As Linus Torvalds 
noted:
>I think, fundamentally, open source does tend to be more stable software. It's the right way to do things.

Our team has a commitment to the mission of open source software because we rely on it so heavily. As a result we contribute 
bug fixes and improvements back to many open source projects that we use internally and encourage our developers to be active 
in open source projects that we use. In recent memory, those that come to mind are:

* [Git] [1]
* [Mercurial] [2]
* [Git Large File Storage] [3]
* [git-flow] [4]
* [Hazelcast] [5]
* [libgit2] [6]
* [Django] [7]
* [Go] [8]
* [Sparkle] [9]

Truth be told though, we've benefited from open source software much more than we've contributed back and while the list above may 
seem impressive, most of our commits have been small bug fixes and/or minor improvements. Looking forward however, we are encouraging 
more open source participation by our developers and hope to increase Atlassian's on-going participation in the world of open source. 
We recognize that as an growing organization, open source is crucial to our business and to the world at large.

 ## Can you describe some of your projects that you have decided to open source?
In addition to contributing to existing projects, the developer tools engineering team is also responsible for creating and open sourcing 
several projects such as:

* [JGit Flow] [10] - A Maven plugin that allows you to use Git Flow and Maven together.
* [JIRA DVCS Connector] [11] - A plugin used to connect JIRA to Git repositories on Bitbucket and GitHub.
* [JIRA FishEye/Bitbucket server plugin] [12] - A plugin used to link commits in FishEye and Bitbucket server with issues in JIRA.
* [Atlassian UI] [13] - The Atlassian user interface library.
* [CommonMark Java] [14] - Java implementation of [CommonMark] [17], a specification of the [Markdown] [18] format for turning plain text into formatted text.
* [git-lob] [15] - An implementation of Git Large File handling that has since been superseded by [git-lfs] [3].
* [Dogslow] [16] - A Django watchdog middleware class that logs tracebacks of slow requests.

We've also released a laundry list of open source plugins and tutorials for our products with [our open source projects hosted on Bitbucket] [19]. We 
are always looking for ways to contribute more to open source projects and intend to increase this trend in the future.

## Does your team have any changes planned in relation to open source?
Probably the biggest open source effort that we have happening right now is around our contribution to the Git Large File storage project. 
Hosting large files in Git repositories is a problem that has prevented some teams for being able to adopt Git and we're working on trying to 
solve it. Obviously we always feel like we can participate much more in the open source community given how much we benefit from it. In the 
next year we'll be encouraging more contributions from our developers back to key projects that we rely on. As we continue to break new ground 
with DVCS, CI/CD, and other innovations we'll be looking to open source some of the components and supporting tools that we build for our internal 
use. Atlassian is known as a company that helps other teams level up when it comes to modern software development practices and as such we do a 
lot of blogging, coaching, and evangelism for the benefit of other organizations, to help them ship great software. While not directly related to 
open source in the traditional sense our technical blog posts on [https://developer.atlassian.com/blog/][20], our extremely popular [Git tutorial site] [21] 
and our [free desktop client for Git and Mercurial] [22] are other ways that we like to give back to the community.

Atlassian supports and believes in the open source mission. Since we benefit so much from open source we make Atlassian software free to use for 
any open source project. It's another way of giving back to the community and hopefully improve the quality of those open source projects that we 
use as well.

[1]: https://git-scm.com/ "Git"
[2]: https://www.mercurial-scm.org/ "Mercurial"
[3]: https://git-lfs.github.com/ 
[4]: https://github.com/nvie/gitflow "git-flow"
[5]: http://hazelcast.org/ "Hazelcast"
[6]: https://libgit2.github.com/ "libgit2"
[7]: https://www.djangoproject.com/ "Django"
[8]: https://golang.org/ "Go"
[9]: http://sparkle-project.org/ "Sparkle"
[10]: http://jgitflow.bitbucket.org/
[11]: https://bitbucket.org/atlassian/jira-dvcs-connector?utm_source=DAC&utm_medium=blog&utm_campaign=os-dev-tools-engineering-blog
[12]: https://bitbucket.org/atlassian/jira-fisheye-plugin?utm_source=DAC&utm_medium=blog&utm_campaign=os-dev-tools-engineering-blog
[13]: https://docs.atlassian.com/aui/latest/?utm_source=DAC&utm_medium=blog&utm_campaign=os-dev-tools-engineering-blog
[14]: https://github.com/atlassian/commonmark-java
[15]: https://github.com/atlassian/git-lob
[16]: https://bitbucket.org/evzijst/dogslow?utm_source=DAC&utm_medium=blog&utm_campaign=os-dev-tools-engineering-blog
[17]: http://commonmark.org/
[18]: https://daringfireball.net/projects/markdown/
[19]: http://atlassian.bitbucket.org/?utm_source=DAC&utm_medium=blog&utm_campaign=os-dev-tools-engineering-blog 
[20]: https://developer.atlassian.com/blog
[21]: https://www.atlassian.com/git/?utm_source=DAC&utm_medium=blog&utm_campaign=os-dev-tools-engineering-blog
[22]: https://www.sourcetreeapp.com/

