---
title: "Tip of the Week - The Atlassian AllStars tips and tricks for JIRA - Part One."
date: "2016-01-07T9:00:00+07:00"
author: "pvandevoorde"
categories: ["totw"]
lede: "In the spirit of starting a new year, I want to share some great tips
and tricks I learned from the Atlassian AllStars. I'm sure you can learn something new too! This week I'll be sharing some JIRA related tips."
---
This week's article is the first part in a series with tips and tricks from real Atlassian users.

We call them the Atlassian AllStars. They are an online community of Atlassian users, experts, and fans.
This community offers opportunities to learn more about our products, to take part in exclusive surveys and interviews, to leverage content and to connect to like-minded fans.

If you think you have what it takes to become an Atlassian AllStar, [apply here](https://atlassian.influitive.com/users/sign_up "Atlassian AllStar Sign up.")!

***

This week we will focus on tips and tricks for JIRA Admins and power users.

###<table><tr><td><img src="rachel-wright.png"/></td><td>[Rachel Wright](https://twitter.com/rlw_www "Rachel Wright") </td></tr></table>

> We know that in JIRA, you can use the ["opsbar-sequence"](https://confluence.atlassian.com/display/JIRA/Advanced+Workflow+Configuration?utm_source=dac&utm_medium=blog&utm_campaign=totw#Advancedworkflowconfiguration-customisingtransitionsonviewissue "opsbar-sequence Confluence page.") property to nicely order the transition buttons. Be sure to:
+ Always order the buttons in their most likely to be used order. (The most likely, "happy path" transition button should be displayed first.)
+ Instead of ordering with values like 1,2,3, use larger values, like 10,20,30. That way, if a new transition is ever needed, you can insert it into the list without having to reorder the existing transitions.

###<table><tr><td>![James Strangeway](james-strangeway.png "James Strangeway")</td><td>[James Strangeway](https://twitter.com/jimmydave23 "James Strangeway")</td></tr></table>

>I like to use the [card colours](https://confluence.atlassian.com/agile063/jira-agile-user-s-guide/configuring-a-board/configuring-card-colours?utm_source=dac&utm_medium=blog&utm_campaign=totw "Card Colours in JIRA Software") in JIRA Software to highlight items that are about to come overdue.  
I will have them turn yellow when they are a day out and then red on the last day before they are due.   There are many ways to use the card colours to bring attention to items, this is just one way I have done it.

###<table><tr><td>![Chris Taylor](chris-taylor.png "Chris Taylor")</td><td>[Chris Taylor](https://twitter.com/AgileTester "Chris Taylor")</td></tr></table>

>Tools like [Atlasboard](http://atlasboard.bitbucket.org/ "Atlasboard Bitbucket repository") are a great way for organizing all your teams data into a custom expandable wallboard! Implementing other great ideas from [ShipIt](https://www.atlassian.com/company/shipit?utm_source=dac&utm_medium=blog&utm_campaign=totw "Atlassian ShipIt") can also help your teams.

###<table><tr><td>![Edson Yanaga](edson-yanaga.png "Edson Yanaga")</td><td>[Edson Yanaga](https://twitter.com/yanaga "Edson Yanaga")</td></tr></table>

>I really like the ability to use the JIRA REST API (I use the [atlassian-provided jar](https://marketplace.atlassian.com/plugins/com.atlassian.jira.jira-rest-java-client/server/overview "REST Java Client for JIRA.")) to read and manipulate JIRA. You can use it in any automation pipeline you want.

###<table><tr><td>![Phill Fox](phill-fox.png "Phill Fox")</td><td> [Phill Fox](https://uk.linkedin.com/in/phillfox "Phill Fox")</td></tr></table>
>Even if you are not using [JIRA Software boards](https://confluence.atlassian.com/agile/jira-agile-user-s-guide/creating-a-board?utm_source=dac&utm_medium=blog&utm_campaign=totw "JIRA Software Board") for your main development methodology, having a Kanban-style board of issues of interest to you is really useful. Mine has 3 swimlanes. Assigned to me; created by me; watched by me.

###<table><tr><td>![Jobin Kuruvilla](jobin-kuruvilla.png "Jobin Kuruvilla")</td><td>[Jobin Kuruvilla](https://twitter.com/jobinkk "Jobin Kuruvilla")</td></tr></table>
>Using the dot(.) and gg [keyboard shortcuts](https://confluence.atlassian.com/jira/using-keyboard-shortcuts-185729624.html?utm_source=dac&utm_medium=blog&utm_campaign=totw "JIRA Keyboard Shortcuts"), which open JIRA's Operations and Administration search dialogs, saves an enormous amount of time for JIRA admins and users.

***

A big thank you to all contributors, you are awesome! Be sure to come back for part two next week!

Share your own tips and tricks in the comments or on Twitter, be sure to mention me: [@pvdevoor](http://twitter.com/pvdevoor "Peter Van de Voorde") or [@atlassiandev](http://twitter.com/atlassiandev "Atlassian Dev Twitter Account") !
