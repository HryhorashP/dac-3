---
title: "Bumpversion is automation for semantic versioning"
date: "2016-02-10T09:30:00+07:00"
author: "ibuchanan"
categories: ["apis", "scripting", "tools"]
lede:          "Semantic versioning (semver) is a scheme for version numbers.
                It also specifies how changes in version numbers
                should convey meaning about the underlying code
                and what has been modified from one version to the next.
                As useful as that is many developers are unaware
                that semver can be easy to maintain with the right tools."
description:   "Ian Buchanan describes
                how you can get started using bumpversion,
                the command-line tool for simplify software releases
                by updating all version strings
                in your source code with the correct increment"
---

[Semantic versioning][semver] (or just semver)
is a specification for communicating about changes in code
from one version to the next.
It targets the problem of [dependency hell][dependency-hell].

> Dependency hell is where you are
> when version lock and/or version promiscuity
> prevent you from easily and safely moving your project forward.

In short, semver specifies the first number should indicate breaking API changes,
the 2nd should indicate added functionality,
and the 3rd should indicate backwards-compatible bugfixes.
With such a strong focus on API changes,
semver is most commonly used for shared libraries.
The rules of semver already help dependency management tools
like [Maven][maven], [NuGet][nuget], and [pip][pip]
understand when libraries are out of date
and when they can be safely updated.
Despite how many developers benefit implicitly from semver,
many are unaware that semver can be easy to maintain with the right tools.

One tool that I like is [bumpversion].
Although it's written in Python,
it can be used in any build process to "bump versions".
On the command-line,
you can use bumpversion like this:

```
bumpversion --current-version 0.5.1 minor src/VERSION
```

That would update the version number `0.5.1` to `0.6.0` in the file `src/VERSION`.
However, if you have already wired your build to use only 1 file,
then you've already made semver easy.
Don't add one more tool just on my say so.
The more compelling usage is with a `.bumpversion.cfg` file in the repo.
It looks something like this:

```ini
[bumpversion]
current_version = 0.5.1

[bumpversion:file:setup.py]
[bumpversion:file:README.rst]
```

This file plays the same role as the previous `src/VERSION` file,
in that it records what the current version is.
But it also stores options for bumpversion
that will propagate the version to other files.
In the above example,
there are version numbers
that need replacing in both `setup.py` and `README.rst`.
I can update both with the simple command:

```shell
bumpversion minor
```

This time I don't have to specify the current version,
or the files that need updating.
When it runs, bumpversion will modify both files,
and the `current_version` variable.
Obviously, now these 3 file changes have to be committed back to Git.
Bumpversion can do that with the `--commit` switch
or the `commit = True` option in the `.bumpversion.cfg` file.
Also, it is convenient to track a release back to the code at that point.
Again, bumpversion can do that,
using the `--tag` switch
or the `tag = True` configuration option.
In either case, don't forget to push when you're done.
I usually wait for the full build process to complete,
including the upload to the appropriate artifact repository,
before I push the changes.
That way I can roll back the changes if things go wrong.
Thanks to my colleage [Nicola Paolucci's treasure trove of git aliases][git-aliases],
I can issue the following commands to roll-back the bump:

```shell
git reset --hard HEAD~1                        # rollback the commit
git tag -d `git describe --tags --abbrev=0`    # delete the tag
```

Or, you can save some time when trying out bumpversion
by using the `--dry-run` option.
That will show you the changes it's going to make
without writing them to disk.
I've only covered the most basic options here,
but there are [options][part-specific-configuration]
to account for alternate versioning schemes,
or additions to standard semver,
like alpha/beta builds or release candidates.

Some parts are still manual,
like _when_ you decide to run bumpversion
and _which kind_ of release bump to make.
Nevertheless, I still find it a big help
to make sure multiple files are updated consistently,
and in accordance with the rules of semver.
I hope you find it useful too.
If you do or have your own tips about semantic versioning,
tweet me at [@devpartisan][devpartisan] or my team at [@atlassiandev][atlassiandev].


[semver]: http://semver.org/ "Tom Preston-Werner (2.0.0). Semantic versioning is a scheme for version numbers and the way they change convey meaning about the underlying code and what has been modified from one version to the next."
[dependency-hell]: https://developer.atlassian.com/blog/2015/01/even-more-continuous-integration-via-dependency-management-tools/ "Ian Buchanan (27 January 2015). Even more continuous integration via dependency management tools."
[maven]: https://maven.apache.org/ "Apache Maven is a software project management and comprehension tool. Based on the concept of a project object model (POM), Maven can manage a project's build, reporting and documentation from a central piece of information."
[nuget]: https://www.nuget.org/ "NuGet is the package manager for the Microsoft development platform including .NET. The NuGet client tools provide the ability to produce and consume packages."
[pip]: https://pip.pypa.io/en/stable/ "Pip is the PyPA recommended tool for installing Python packages."
[bumpversion]: https://pypi.python.org/pypi/bumpversion "Bumpversion is a small command line tool to simplify releasing software by updating all version strings in your source code by the correct increment."
[git-aliases]: https://bitbucket.org/durdn/cfg/src/master/.gitconfig?fileviewer=file-view-default "Dotfiles for Nicola Paolucci."
[part-specific-configuration]: https://pypi.python.org/pypi/bumpversion#part-specific-configuration "Part specific configuration lets you override the semver defaults for bumpversion."
[devpartisan]: https://twitter.com/devpartisan "Ian Buchanan is @devpartisan on Twitter."
[atlassiandev]: https://www.twitter.com/atlassiandev "Atlassian's Developer Advocates for Developer Tools are @atlassiandev on Twitter."
