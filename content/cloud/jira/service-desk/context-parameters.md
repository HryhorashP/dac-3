---
title: Context parameters
platform: cloud
product: jsdcloud
category: devguide
subcategory: blocks
date: "2016-10-31"
---
{{< include path="content/cloud/connect/concepts/context-parameters.snippet.md">}}
