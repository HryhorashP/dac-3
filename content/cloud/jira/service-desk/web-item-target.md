---
title: Web item target
platform: cloud 
product: jsdcloud
category: reference
subcategory: modules
aliases:
- /jiracloud/jira-service-desk-modules-web-item-target-40004227.html
- /jiracloud/jira-service-desk-modules-web-item-target-40004227.md
confluence_id: 40004227
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=40004227
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=40004227
date: "2016-06-23"
---
# Web item target

The web item target module defines the way a web item link is opened in the browser, such as in a page or modal dialog.

#### Sample JSON

``` json
{
  ...
  "target": {
    "type": "dialog"
  }
}
```

#### Properties

`type`

-   **Type**: `string`
-   **Allowed values**: page, dialog
-   **Default**: page
-   **Description**: Defines how the web-item content should be loaded by the page. By default, the web-item is loaded in the same page. The target can be set to open the web-item url as a modal dialog.
