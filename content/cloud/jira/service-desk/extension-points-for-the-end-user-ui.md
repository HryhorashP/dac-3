---
title: "Extension points for the end-user UI"
platform: cloud
product: jsdcloud
category: reference
subcategory: modules
date: "2016-11-01"
---

{{< reuse-page path="content/cloud/jira/platform/extension-points-for-the-end-user-ui.md">}}