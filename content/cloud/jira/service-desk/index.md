---
title: Integrating with JIRA Service Desk Cloud
platform: cloud
product: jsdcloud
category: devguide
subcategory: index
aliases:
- /jiracloud/jira-service-desk-cloud-development-39981106.html
- /jiracloud/jira-service-desk-cloud-development-39981106.md
confluence_id: 39981106
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39981106
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39981106
date: "2016-09-20"
---
# Integrating with JIRA Service Desk Cloud

Welcome to JIRA Service Desk Cloud development! This overview will cover everything you need to know to integrate with JIRA Cloud. This includes the Atlassian Connect framework, which is used to integrate with Atlassian Cloud applications, as well as JIRA Service Desk features and services that you can use when building an add-on.

{{% tip title="Hello world"%}}If you already know the theory and want to jump straight into development, read our [Getting started guide](/cloud/jira/service-desk/getting-started) to build your first JIRA Cloud add-on.{{% /tip %}}

<br>
We recommend that you read this entire page if you haven't integrated with JIRA Service Desk Cloud before, but if you want to jump to a specific section, use the links below:

-	[What is JIRA Service Desk?](#overview)
-   [JIRA Service Desk Cloud and Atlassian Connect](#jira-service-desk-cloud-and-atlassian-connect)
-   [Building blocks for integrating with JIRA Service Desk Cloud](#building-blocks-for-integrating-with-jira-service-desk-cloud) *(REST API, webhooks, and modules)*
-   [JIRA Service Desk Cloud and the JIRA platform](#jira-service-desk-cloud-and-the-jira-platform)
-   [Looking for inspiration?](#inspiration)
-   [More information](#more-information)

## What is JIRA Service Desk? <a name="overview"></a>

JIRA Service Desk is primarly used as a service solution, from asset management to DevOps. A diverse range of IT teams use JIRA Service Desk, including support desk teams, operations teams, and more. With over 15,000 of these teams using JIRA Service Desk, there's plenty of potential to extend it. Jump in and get started!

{{< youtube 5u62pSDVB_I >}} 

 If you haven't used JIRA Service Desk before, check out the [product overview] for more information.

## JIRA Service Desk Cloud and Atlassian Connect

If you want to integrate with any JIRA Cloud product, including JIRA Service Desk Cloud, then you should use Atlassian Connect. Atlassian Connect is an extensibility framework that handles discovery, installation, authentication, and seamless integration into the JIRA UI. An Atlassian Connect add-on could be an integration with another existing service, new features for JIRA, or even a new product that runs within JIRA. 

If you haven't used Atlassian Connect before, check out the [Getting started guide]. This guide will help you learn how to set up a development environment and build a JIRA Cloud add-on.

## Building blocks for integrating with JIRA Service Desk Cloud

The three building blocks of integrating with JIRA Service Desk are the REST API, webhooks, and modules.

### JIRA Service Desk Cloud REST API

|              |                 |
|--------------|-----------------|
| ![](../../../illustrations/atlassian-software-47.png) | The JIRA Service Desk Cloud REST API lets your add-on communicate with JIRA Service Desk Cloud. For example, using the REST API, you can retrieve a queue's requests to display in your add-on or create requests from phone calls.<br>See the [JIRA Service Desk Cloud REST API] reference for details.<br>*Note, JIRA Service Desk is built on the JIRA platform, so you can also use the [JIRA Cloud platform REST API] to interact with JIRA Service Desk Cloud.* |

### Webhooks and automation rules

|              |                 |
|--------------|-----------------|
| ![](../../../illustrations/atlassian-software-46.png) | Add-ons and applications can react to conditions/events in JIRA Service Desk via automation rules. You can implement an "automation action" that performs actions in a remote system as part of an automation rule. An automation rule can also be configured to fire a webhooks that notifies your add-on or application.<br> For more information, see [JIRA Service Desk webhooks].|

### JIRA Service Desk modules

|              |                 |
|--------------|-----------------|
| ![](../../../illustrations/atlassian-software-52.png) | A module is simply a UI element, like a tab or a menu. JIRA Service Desk UI modules allow add-ons to interact with the JIRA Service Desk UI. For example, your add-on can use a JIRA Service Desk UI module to add a panel to the top of customer portals.<br>For more information, see [About JIRA modules].|


## JIRA Service Desk Cloud and the JIRA platform

JIRA Service Desk is an application built on the JIRA platform. The JIRA platform provides a set of base functionality that is shared across all JIRA applications, like issues, workflows, search, email, and more. A JIRA application is an extension of the JIRA platform that provides specific functionality. For example, JIRA Service Desk adds customer request portals, support queues, SLAs, a knowledge base, and automation.

This means that when you develop for JIRA Service Desk, you are actually integrating with the JIRA Service Desk application as well as the JIRA platform. The JIRA Service Desk application and JIRA platform each have their own REST APIs, webhook events, and web fragments. 

Read the [JIRA Cloud platform documentation] for more information.

## Looking for inspiration? <a name="inspiration"></a>

If you are looking for ideas on building the next JIRA Service Desk Cloud integration, the following use cases and examples may help.

Here are some common JIRA Service Desk use cases:

-   **Support helpdesk**: An easy way to provide support to anyone in the organization with IT requests such as hardware (laptop) requests, or software requests. 
-   **ITSM/ITIL:** More advanced IT teams want to use a service solution to support ITSM and ITIL processes, including incident, problem, and change management. 
-   **Asset management:** IT teams want to discover, control, monitor, and track key IT assets such as hardware and servers. 
-   **DevOps:** Developer, Operations, and IT teams can use JIRA Service Desk to collaborate together and solve problems faster. 
-   **Business teams:** Finance and HR teams may want to use JIRA Service Desk to collect requests from anyone in the organization. 

Here are a few examples of what you can build on top of JIRA Service Desk:

- **Customer portal customization** -- JIRA Service Desk provides an intuitive customer portal that makes it easy for non-technical end users to interact with service teams like IT and support. By extending this, you can build a completely tailored interface for the customer portal that matches your company's branding.
- **Collect requests outside of JIRA Service Desk** -- Build functionality to create requests on behalf of customers in any number of ways. For example, integrate it into the support section of your website, or have a get help menu on your mobile app, or hook up alerts from a system monitoring tool to create incidents in JIRA Service Desk.
- **SLA integration** -- JIRA Service Desk introduces the notion of service level agreements (SLAs) by letting teams accurately measure and set goals based on time metrics, e.g. time to assign, time to respond, time to resolution. With the JIRA Service Desk REST API, you can now get detailed SLA information and create your own reports. 
- **Telephony integration** -- Create requests based on incoming voice calls by integrating your telephony system with JIRA Service Desk, via the REST API.
- **Supplement request information** -- Add information about assets, the customer, or other relevant information to requests to make it easier for agents to solve problems and close requests.


## More information

-   [JIRA Service Desk tutorials](../tutorials-and-guides.html) -- Learn more about JIRA Service Desk development by trying one of our hands-on tutorials.
-   [jira-servicedesk-development tag] on the Atlassian Answers forum -- Join the discussion on JIRA Service Desk development.

  [product overview]: https://www.atlassian.com/software/jira/service-desk
  [Getting started guide]: /cloud/jira/platform/getting-started
  [JIRA Service Desk Cloud REST API]: https://docs.atlassian.com/jira-servicedesk/REST/cloud/
  [JIRA Cloud platform REST API]: https://docs.atlassian.com/jira/REST/cloud/
  [JIRA Service Desk webhooks]: /cloud/jira/service-desk/jira-service-desk-webhooks
  [About JIRA modules]: /cloud/jira/service-desk/about-jira-modules
  [JIRA Cloud platform documentation]: /cloud/jira/platform/integrating-with-jira-cloud
  [JIRA Service Desk tutorials]: /cloud/jira/service-desk/learning
  [Atlassian Connect documentation]: https://developer.atlassian.com/static/connect/docs/latest/index.html
  [jira-servicedesk-development tag]: https://answers.atlassian.com/questions/topics/31850522/jira-servicedesk-development
