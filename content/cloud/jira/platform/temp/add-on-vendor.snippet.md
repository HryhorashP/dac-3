## Add-on vendor

Gives basic information about the add-on vendor

``` json
{
  "vendor": {
    "name": "Atlassian",
    "url": "http://www.atlassian.com"
  }
}
```

### Properties
<div class="ac-properties"><a id="name"></a><h3><code>name</code></h3><div class="aui-group"><div class="aui-item ac-property-key"><h5>Type</h5></div><div class="aui-item"><code>string</code></div></div><div class="aui-group"><div class="aui-item ac-property-key"><h5>Description</h5></div><div class="aui-item"><p>The name of the add-on vendor.
 Supply your name or the name of the company you work for.</p></div></div><a id="url"></a><h3><code>url</code></h3><div class="aui-group"><div class="aui-item ac-property-key"><h5>Type</h5></div><div class="aui-item"><code>string</code><p></p><code>uri</code></div></div><div class="aui-group"><div class="aui-item ac-property-key"><h5>Description</h5></div><div class="aui-item"><p>The url for the vendor's website</p></div></div></div>