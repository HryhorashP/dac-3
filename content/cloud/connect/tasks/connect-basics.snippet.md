# Connect basics

This Hello World tutorial shows you a basic preview of the Connect framework. Connect add-ons are essentially web 
applications that integrate with your Atlassian product. In this tutorial, you'll turn a simple web application into an 
Atlassian Connect add-on. You'll install it in [your JIRA Cloud development environment][1], 
and access your add-on from a link in the header. Your add-on displays as an iframe of your web application.

Here's what you'll accomplish:

* [Create a basic `atlassian-connect.json` descriptor](#descriptor)  
* [Build an add-on web application: A super simple HTML page](#webapp)  
* [Get a JIRA development environment](#runjira)  
* [Install and test your add-on](#install)

By the end, you'll see your web application displayed in an iframe inside of JIRA.

## <a name="descriptor"></a>Create the add-on descriptor (`atlassian-connect.json`)

In this step you will create a JSON descriptor file. This file describes your add-on to the Atlassian application, which in this case is JIRA Cloud. Your descriptor specifies your add-on's key, name, permissions needed to operate, and the different modules it uses for integration.

Your `atlassian-connect.json` file will use a [`generalPages` module](/cloud/jira/platform/modules/page.html), and add a link to JIRA's top navigation element titled "Greeting".

1. Create a project directory for your add-on source files.  
    You'll work in this directory for the duration of this tutorial.
2. In your project directory, create a new file named `atlassian-connect.json`.
3. Add the following text to the file:
``` json
    {
        "name": "Hello World",
        "description": "Atlassian Connect add-on",
        "key": "com.example.myaddon",
        "baseUrl": "https://<my-addon-url>",
        "vendor": {
            "name": "Example, Inc.",
            "url": "http://example.com"
        },
        "authentication": {
            "type": "none"
        },
        "apiVersion": 1,
        "modules": {
            "generalPages": [
                {
                    "url": "/helloworld.html",
                    "key": "hello-world",
                    "location": "system.top.navigation.bar",
                    "name": {
                        "value": "Greeting"
                    }
                }
            ]
        }
    }
```
    __Note:__ "baseUrl" attribute must be unique to each add-on
4. Save and close the descriptor file.  

__Note:__ You can validate your descriptor using this [handy tool](https://atlassian-connect-validator.herokuapp.com/validate).

## <a name="webapp"></a>Create a simple web application to stand in as an add-on

Now, you're ready to create the web app. You'll use a simple, old-fashioned HTML page as an "app" to demonstrate how Connect integrates with your application. While a static HTML page doesn't represent a typical add-on, it's not that far off either. Just a few components turn any web application into an Atlassian Connect add-on.

Your simple content will use [AUI (Atlassian User Interface) styling](../concepts/styling-with-aui.html). You'll add two key pieces to an HTML file: a `script` tag, and an `ac-content` wrapper class. 

<table class="aui">
    <thead>
        <tr>
            <th>Element</th>
            <th>Details</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><strong><tt>script</tt></strong></td>
            <td>
                <p>This element is comprised of 2 values pointing toward <tt>all.js</tt>, formatted as <tt>//HOSTNAME.atlassian.net/CONTEXT/atlassian-connect/all.js</tt>. These values are provided in the URL of the request for this resource.</p>
                <p>Let's look at the components:</p>
                <ul>
                    <li><tt>HOSTNAME</tt>: The hostname for the Atlassian application.</li>
                    <li><tt>CONTEXT</tt>: The application context for the application. Note that JIRA is the default context (when none is provided) and <tt>wiki</tt> is used for Confluence.</li>
                    <li><tt>all.js</tt>: This file is available in any Atlassian application that supports Connect. This <a href="../concepts/javascript-api.html">Javascript API library</a> provides functions you can use for your add-on. In this case, it enables iframe resizing for the JIRA page that displays your add-on.</li>
                </ul>
        </tr>
        <tr>
            <td><strong><tt>ac-content</tt></strong></td>
            <td>This class wraps the content of your add-on, and dynamically resizes the iframe in JIRA. This keeps your add-on content visible without pesky scrollbars.</td>
        </tr>
    </tbody>
</table>  

From the same project directory:

1. Create the page you referenced in the `url` element in your descriptor file, `helloworld.html`.
2. Add the following content:
``` html
<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="stylesheet" href="//aui-cdn.atlassian.com/aui-adg/5.9.12/css/aui.min.css" media="all">
    </head>
    <body>
        <section id="content" class="ac-content">
            <div class="aui-page-header">
                <div class="aui-page-header-main">
                    <h1>Hello World</h1>
                </div>
            </div>
        </section>
        
        <script id="connect-loader" data-options="sizeToParent:true;">
            (function() {
                var getUrlParam = function (param) {
                    var codedParam = (new RegExp(param + '=([^&]*)')).exec(window.location.search)[1];
                    return decodeURIComponent(codedParam);
                };
            
                var baseUrl = getUrlParam('xdm_e') + getUrlParam('cp');
                var options = document.getElementById('connect-loader').getAttribute('data-options');
            
                var script = document.createElement("script");
                script.src = baseUrl + '/atlassian-connect/all.js';
              
                if(options) {
                    script.setAttribute('data-options', options);
                }
              
                document.getElementsByTagName("head")[0].appendChild(script);
            })();
        </script>
        
    </body>
</html>
```  

<div class="aui-message">
Within your iframe, you will always need to get the all.js script from the product domain.
The code within the script tag above, is an easy way to do this without a having to re-create the html through a template on each call.  

For more information on this, check out the [Cookbook](../concepts/javascript-api.html#all.js)
</div>

## <a name="start-addon-host" id="start-addon-host"></a> Start your add-on

That's it as far as coding goes. The next step is to make your files available on a web
server. There are many ways to do this, but in this example you'll serve the file locally,
and use [ngrok](http://ngrok.com) to make this available on the internet.

You'll use a simple web server to serve the current directory containing your 
`atlassian-connect.json` and `helloworld.html` files.

1. From the same directory, start your server on port 8000:
     ``` shell
     npm install http-server -g
     ```
http-server -p 8000</code></pre>
    The server indicates that it's serving HTTP at the current address and port. You'll see something like this:
    <tt>Starting up http-server, serving ./ on: http://0.0.0.0:8000</tt>
1. Confirm the files you created in steps 1 and 2 are served. Visit:
    * <code data-lang="text"><a href="http://localhost:8000/atlassian-connect.json">http://localhost:8000/atlassian-connect.json</a></code>
    * <code data-lang="text"><a href="http://localhost:8000/helloworld.html">http://localhost:8000/helloworld.html</a></code>
1. Make this add-on avaliable on the public internet via `ngrok`. To see how to do this read through the [developing locally guide][2]. 

## <a name="runjira"></a>Get a JIRA development environment

You've created the essential components of a Connect add-on: an `atlassian-connect.json` descriptor file to communicate 
what your add-on does to JIRA, and a web application (`helloworld.html`) running on a local server.

Now you need to test what you have written in JIRA. For instructions on how to do this, go to the 
[Development Setup][1] page. 

You will also need to ensure that your add-on can be reached by JIRA (i.e. hosted on the web).
For an easy way to do this on your local machine, follow the instructions on [Developing locally][2].

## <a name="install"></a>Install your add-on in JIRA

Now you're ready to install your add-on in your development version of JIRA. In this step, you'll navigate to the 
[Universal Plugin Manager (UPM)](https://confluence.atlassian.com/x/8AJTE) and add a link to your descriptor file.

When you install your add-on, JIRA retrieves and registers your `atlassian-connect.json` descriptor. 

1. From JIRA, choose __Cog Menu > Add-ons__ from the top navigation menu.

2. Click __Manage add-ons__ from the side menu.

3. Click __Upload add-on__ from the right side of the page.

4. Insert a link to your add-on descriptor, hosted somewhere on the internet via https.  
     This URL should match the hosted location of your `atlassian-connect.json` descriptor file.

5. Click __Upload__.  
    JIRA displays the *Installed and ready to go* dialog when installation is complete.

6. Click __Close__.

7. Verify that your add-on appears in the list of *User installed add-ons*.
    For example, if you used Hello World for your add-on name, *Hello World* should appear in the list.

8. Reload the page.

9. Click __Greeting__ in the application header.  
    Your *Hello World* message appears on the page.  

## What's next?

So far, you've learned the basic architecture of a Connect add-on. The next step is to add some functionality and handle [authentication](../authentication-for-add-ons.html).

You can add functionality using the [Atlassian REST APIs](https://developer.atlassian.com/x/K4BpAQ). [Authentication](../authentication-for-add-ons.html) manages the handshake between your app and the Atlassian host application. You can also read about our [security concepts](../concepts/security.html) for more information.

You can also try the next tutorial, where you can add a table of your projects to JIRA via the REST API and D3.js. 

<div class="index-button">
<a href="../tutorials/project-activity-tutorial.html"><button class="primary-cta aui-button aui-button-primary">Try the next tutorial</button></a>
</div>

## More resources

### Example add-ons

We have a few [sample applications](../resources/samples.html) you can reference. These example add-ons demonstrate
authentication and many other patterns you can use to develop your own add-ons.

### Join the Connect community

Explore Connect topics on [Atlassian Answers](https://answers.atlassian.com/tags/atlassian-connect). We also encourage you to join our [Connect mailing list](https://groups.google.com/forum/?fromgroups=#!forum/atlassian-connect-dev).

 [1]: ../guides/development-setup.html
 [2]: ../developing/developing-locally-ngrok.html
