
# Connect cookbook: Confluence

This cookbook is an extension of the [JavaScript API Cookbook](./javascript-api.html#cookbook),
which provides snippets of code that can be used in the clients browser to get information from the product.  

This document provides snippets specific to __Confluence__, covering:

* [Getting a list of Confluence spaces](#get-spaces)
* [Getting information about specific spaces](#get-specific-space)  
* [Getting Confluence space pages](#getting-space-pages)  


### <a name="get-spaces"></a> Getting Confluence spaces

This retrieves a list of spaces from Confluence, and may require paging through the results to see all 
spaces from your instance.

``` javascript
AP.require('request', function(request) {
  request({
    url: '/rest/api/space',
    success: function(response) {
      // convert the string response to JSON
      response = JSON.parse(response);

      // dump out the response to the console
      console.log(response);
    },
    error: function() {
      console.log(arguments);
    }  
  });
});
```

### <a name="get-specific-space"></a> Getting specific spaces from Confluence

This snippet lets you request a specific Confluence space by space key. In this example, we use `ds`. This recipe 
also provides some high-level information about the space. If you're looking for more information about 
a space, you can find out about the content in the space in the next example, using `/rest/api/space/{space.key}/content`.


``` javascript
AP.require('request', function(request) {
  request({
    url: '/rest/api/space/ds',
    success: function(response) {
      // convert the string response to JSON
      response = JSON.parse(response);

      // dump out the response to the console
      console.log(response);
    },
    error: function() {
      console.log(arguments);
    }   
  });
});
```

### <a name="getting-space-pages"></a> Getting pages in a space

This recipe returns a collection for a given space identifier (like `ds` in the example below), containing 
objects like a blog post and one of the pages in the space.
This returns a collection for the given space identifier (e.g. `ds`) that contains a object of blog posts and one of
pages in the space. You can also directly access pages (`/rest/api/space/ds/content/page`) or 
blog posts (`/rest/api/space/ds/content/blogpost`) if you want to page though the contents. 

``` javascript
var space
AP.require('request', function(request) {
  request({
    url: '/rest/api/space/ds/content',
    success: function(response) {
      // convert the string response to JSON
      response = JSON.parse(response);

      // dump out the response to the console
      console.log(response);
    },
    error: function() {
      console.log(arguments);
    }    
  });
});
```