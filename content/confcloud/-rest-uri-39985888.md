---
title: Rest Uri 39985888
aliases:
    - /confcloud/-rest-uri-39985888.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39985888
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39985888
confluence_id: 39985888
platform:
product:
category:
subcategory:
---
# Confluence Connect : \_REST URI

URIs for a Confluence REST API resource have the following structure:
With context:

``` syntaxhighlighter-pre
http://example.com:1234/context/rest/api-name/api-version/resource-name
```

Or without context:

``` syntaxhighlighter-pre
http://example.com:1234/rest/api-name/api-version/resource-name
```

![(info)] In Confluence 3.1 and Confluence 3.2, the only available `api-name` is `prototype`.
**Examples:**
With context:

``` syntaxhighlighter-pre
http://example.com:8080/confluence/rest/prototype/1/space/ds
http://localhost:8080/confluence/rest/prototype/latest/space/ds
```

Or without context:

``` syntaxhighlighter-pre
http://confluence.example.com:8095/rest/prototype/1/space/ds
http://confluence.example.com:8095/rest/prototype/latest/space/ds
```

##### RELATED TOPICS

[Confluence REST APIs - Prototype Only]

  [(info)]: /confcloud/images/icons/emoticons/information.png
  [Confluence REST APIs - Prototype Only]: https://developer.atlassian.com/display/CONFDEV/Confluence+REST+APIs+-+Prototype+Only

