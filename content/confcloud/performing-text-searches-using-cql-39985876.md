---
title: Performing Text Searches Using Cql 39985876
aliases:
    - /confcloud/performing-text-searches-using-cql-39985876.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39985876
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39985876
confluence_id: 39985876
platform:
product:
category:
subcategory:
---
# Confluence Connect : Performing text searches using CQL

This page provides information on how to perform text searches. It applies to [advanced searches] when used with the [CONTAINS] operator.

Acknowledgements:

Confluence uses Apache Lucene for text indexing, which provides a rich query language. Much of the information on this page is derived from the <a href="http://lucene.apache.org/core/4_4_0/queryparser/org/apache/lucene/queryparser/classic/package-summary.html#Term_Modifiers" class="external-link">Query Parser Syntax</a> page of the Lucene documentation.

## Query terms

A query is broken up into **terms** and **operators.** There are two types of terms: **Single Terms** and **Phrases.**

A **Single Term** is a single word such as "`test`" or "`hello`".

A **Phrase** is a group of words surrounded by double quotes such as "`hello dolly`".

*Note: All query terms in Confluence are case insensitive.*

## Term modifiers

Confluence supports modifying query terms to provide a wide range of searching options.

**On this page:**

-   [Query terms]
-   [Term modifiers]

**Related topics:**

-   [Advanced Searching using CQL]

[Wildcard searches: ? and \*] | [Fuzzy searches: ~]

### Wildcard searches: ? and \*

Confluence supports single and multiple character wildcard searches.

To perform a single character wildcard search use the "`?`" symbol.

To perform a multiple character wildcard search use the "`*`" symbol.

Wildcard characters need to be enclosed in quote-marks, as they are reserved characters in [advanced search][advanced searches]. Use quotations, e.g. `summary ~ "cha?k and che*"`

The single character wildcard search looks for terms that match that with the single character replaced. For example, to search for "`text`" or "`test`" you can use the search:

``` syntaxhighlighter-pre
te?t
```

Multiple character wildcard searches looks for 0 or more characters. For example, to search for `Windows`, `Win95` or `WindowsNT` you can use the search:

``` syntaxhighlighter-pre
win*
```

You can also use the wildcard searches in the middle of a term. For example, to search for `Win95` or `Windows95` you can use the search

``` syntaxhighlighter-pre
wi*95
```

You cannot use a \* or ? symbol as the first character of a search. The feature request for this is <a href="https://jira.atlassian.com/browse/JRA-6218" class="external-link">JRA-6218</a>

### Fuzzy searches: ~

Confluence supports fuzzy searches. To do a fuzzy search use the tilde, "~", symbol at the end of a single word term. For example to search for a term similar in spelling to "`roam`" use the fuzzy search:

``` syntaxhighlighter-pre
roam~
```

This search will find terms like foam and roams.

Note: terms found by the fuzzy search will automatically get a boost factor of 0.2

  [advanced searches]: /confcloud/performing-text-searches-using-cql-39985876.html
  [CONTAINS]: #PerformingtextsearchesusingCQL-CONTAINS
  [Query terms]: #PerformingtextsearchesusingCQL-Queryterms
  [Term modifiers]: #PerformingtextsearchesusingCQL-Termmodifiers
  [Advanced Searching using CQL]: /confcloud/advanced-searching-using-cql-39985862.html
  [Wildcard searches: ? and \*]: #PerformingtextsearchesusingCQL-Wildcardsearches:?and*
  [Fuzzy searches: ~]: #PerformingtextsearchesusingCQL-Fuzzysearches:~

