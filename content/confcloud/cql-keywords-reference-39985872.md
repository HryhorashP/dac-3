---
title: Cql Keywords Reference 39985872
aliases:
    - /confcloud/cql-keywords-reference-39985872.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39985872
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39985872
confluence_id: 39985872
platform:
product:
category:
subcategory:
---
# Confluence Connect : CQL Keywords Reference

A keyword in CQL is a word or phrase that:

-   joins two or more clauses together to form a complex CQL query, or
-   alters the logic of one or more clauses, or
-   alters the logic of [operators], or
-   has an explicit definition in a CQL query, or
-   performs a specific function that alters the results of a CQL query.

**List of Keywords:**

-   [AND]
-   [OR]
-   [NOT]
-   [ORDER BY]

#### AND

Used to combine multiple clauses, allowing you to refine your search.

Note: you can use [parentheses] to control the order in which clauses are executed.

###### Examples

-   Find all blogposts with the label "performance"

    ``` syntaxhighlighter-pre
    label = "performance" and type = "blogpost"
    ```

-   Find all pages created by jsmith in the DEV space

    ``` syntaxhighlighter-pre
    type = page and creator = jsmith and space = DEV
    ```

-   Find all content that mentions jsmith but was not created by jsmith

    ``` syntaxhighlighter-pre
    mention = jsmith and creator != jsmith
    ```

[^top of keywords] | [^^top of topic]

#### OR

Used to combine multiple clauses, allowing you to expand your search.

Note: you can use [parentheses] to control the order in which clauses are executed.

(Note: also see [IN], which can be a more convenient way to search for multiple values of a field.)

###### Examples

-   Find all content in the IDEAS space or with the label idea

    ``` syntaxhighlighter-pre
    space = IDEAS or label = idea
    ```

-   Find all content last modified before the start of the year or with the label needs\_review

    ``` syntaxhighlighter-pre
    lastModified < startOfYear() or label = needs_review
    ```

[^top of keywords] | [^^top of topic]

#### NOT

Used to negate individual clauses or a complex CQL query (a query made up of more than one clause) using [parentheses], allowing you to refine your search.

(Note: also see [NOT EQUALS] ("!="), [DOES NOT CONTAIN] ("!~") and [NOT IN].)

###### Examples

-   Find all pages with the "cql" label that aren't in the dev space

    ``` syntaxhighlighter-pre
    label = cql and not space = dev 
    ```

[^top of keywords] | [^^top of topic]

#### ORDER BY

Used to specify the fields by whose values the search results will be sorted.

By default, the field's own sorting order will be used. You can override this by specifying ascending order ("`asc`") or descending order ("`desc`").

Not all fields support Ordering. Generally, ordering is not supported where a piece of content can have multiple values for a field, for instance ordering is not supported on labels.

###### Examples

-   Find content in the DEV space ordered by creation date

    ``` syntaxhighlighter-pre
    space = DEV order by created
    ```

-   Find content in the DEV space ordered by creation date with the newest first, then title

    ``` syntaxhighlighter-pre
    space = DEV order by created desc, title
    ```

-   Find pages created by jsmith ordered by space, then title

    ``` syntaxhighlighter-pre
    creator = jsmith order by space, title asc
    ```

[^top of keywords] | [^^top of topic]

  [operators]: #CQLKeywordsReference-operators
  [AND]: #CQLKeywordsReference-AND
  [OR]: #CQLKeywordsReference-OR
  [NOT]: #CQLKeywordsReference-NOT
  [ORDER BY]: #CQLKeywordsReference-ORDERBY
  [parentheses]: #CQLKeywordsReference-parentheses
  [^top of keywords]: #CQLKeywordsReference-keywords
  [^^top of topic]: #CQLKeywordsReference-top
  [IN]: #CQLKeywordsReference-IN
  [NOT EQUALS]: #CQLKeywordsReference-NOT_EQUALS
  [DOES NOT CONTAIN]: #CQLKeywordsReference-DOES_NOT_CONTAIN
  [NOT IN]: #CQLKeywordsReference-NOT_IN

