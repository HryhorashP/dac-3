---
title: Macro Autoconvert with Confluence Connect 41224402
aliases:
    - /confcloud/macro-autoconvert-with-confluence-connect-41224402.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=41224402
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=41224402
confluence_id: 41224402
platform:
product:
category:
subcategory:
---
# Confluence Connect : Macro Autoconvert with Confluence Connect

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Description</td>
<td>Add autoconvert patterns to your macro experience.</td>
</tr>
<tr class="even">
<td>Level</td>
<td><div class="content-wrapper">
<p>3 - INTERMEDIATE</p>
</div></td>
</tr>
<tr class="odd">
<td>Estimated Time</td>
<td>15 minutes</td>
</tr>
</tbody>
</table>

# Prerequisites

Ensure you have worked through the [Quick Start Guide for Confluence Connect].

# Macro Magic

Macros are arguably the most powerful capabilities provided by the Confluence Editor. As an add-on developer, building a macro is a quick process, as outlined in our [Quick Start Guide][Quick Start Guide for Confluence Connect]. However, Confluence Connect macros come bundled with extra behaviours and customisations - ensuring you are able to develop and guide users through the add-on experience you desire.

Let's take a quick look at macro autoconvert - a feature which allows for intelligent insertion of macros to the Confluence Editor (below is also a sneak peak of the [Macro Custom Editor with Confluence Connect]).

 

<img src="/confcloud/attachments/41224402/41224417.gif" class="image-center" width="1200" />

So, what happened here?

-   **Autoconvert:
    **We pasted the link - <a href="http://giphy.com/gifs/l3E6xxDZC4AADCAE0" class="uri" class="external-link">http://giphy.com/gifs/l3E6xxDZC4AADCAE0</a> - into our Confluence page, and it auto-created an instance of the GIPHY macro, with this URL set. 
     

As a developer, this gives you extra control over the behaviour of the macros you introduce into Confluence. Pretty awesome, right? Let's take a look at how to do this. ** **

If you haven't already built a basic GIPHY macro, check out the [Quick Start Guide for Confluence Connect]! It'll only take you a few minutes!

 

# From URL to Macro

Autoconvert works in a simple but powerful way.

By declaring URL patterns in our atlassian-connect.json descriptor, Confluence ensures that any pasted URLs which match these patterns are autoconverted to an instance of our macro, with a specific parameter set. This occurs by registering specific "matchers" in our macro module descriptor.

For example, if we specify a pattern of "<a href="http://giphy.com/" class="uri" class="external-link">http://giphy.com/</a>{}" and relate this to the "url" parameter of our GIPHY macro, all pasted URLs which match this pattern will be converted to an instance of our GIPHY macro with the url set to "<a href="http://giphy.com/" class="uri" class="external-link">http://giphy.com/</a>{}".

"{}" denotes a wildcard pattern match. This allows the above pattern to match both of these urls:

-   <a href="http://www.youtube.com/123" class="external-link">http://giphy.com/123</a>
-   <a href="http://www.youtube.com/abc123" class="external-link">http://giphy.com/abc123</a>

If we were to for example, have a URL like [http://giphy.com/{}/]{}, this would match:

-   <a href="http://giphy.com/123/456" class="uri" class="external-link">http://giphy.com/123/456</a>
-   <a href="http://giphy.com/hello/world" class="uri" class="external-link">http://giphy.com/hello/world</a>

You can use as many wildcards in the same pattern, allowing for some complex URL matching!

The magic happens in our plugin descriptor. Add the following to the GIPHY module defined under 'dynamicContentMacros': 

``` syntaxhighlighter-pre
{
  "autoconvert": {
    "urlParameter": "url",
    "matchers": [
      {
        "pattern": "http://giphy.com/gifs/{}"
      }
    ]
  }
}
```

*The key to remember here is, if we are specifying that the "urlParameter" is "url" as above, we **must have** a parameter with an "identifier" of "url" in our plugin descriptor.*

For the above example we may have the following macro parameter defined: 

``` syntaxhighlighter-pre
"parameters": [
  {
    "identifier": "url",
    "name": {
      "value": "URL"
    },
    "description": {
      "value": "Giphy URL."
    },
    "type": "string",
    "required": true
  }
]
```

We must also ensure that the rest of our macro parameters are not required (or in other words, have default parameter values), otherwise the macro editor will appear when a URL is pasted.

 

To see an example of more complex URL matching in play for GIPHY, check out the full <a href="https://bitbucket.org/atlassian/confluence-giphy-addon/src/5d34957b0c2769d0f4202ee848e94a77e88d03e8/atlassian-connect.json?fileviewer=file-view-default#atlassian-connect.json-68" class="external-link">atlassian-connect.json descriptor here!</a>

# Creating a Macro through Autoconvert

What more is involved here?

Nothing, that's it! Try pasting a link such as <a href="http://giphy.com/gifs/applause-clapping-clap-aLdiZJmmx4OVW" class="uri" class="external-link">http://giphy.com/gifs/applause-clapping-clap-aLdiZJmmx4OVW</a> into your Confluence editor. You should see the following: 
 

<img src="/confcloud/attachments/41224402/41224404.png" class="image-center" width="600" height="250" />

 

Simple and awesome!

  [Quick Start Guide for Confluence Connect]: https://developer.atlassian.com/confcloud/quick-start-to-confluence-connect-39987884.html
  [Macro Custom Editor with Confluence Connect]: /confcloud/macro-custom-editor-with-confluence-connect-41224399.html
  [http://giphy.com/{}/]: 

