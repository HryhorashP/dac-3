---
title: Cql Function Reference 39985867
aliases:
    - /confcloud/cql-function-reference-39985867.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39985867
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39985867
confluence_id: 39985867
platform:
product:
category:
subcategory:
---
# Confluence Connect : CQL Function Reference

The instructions on this page describe how to use functions in CQL to define structured search queries to [search for content in Confluence].  

## Functions Reference

A function in CQL appears as a word followed by parentheses which may contain one or more explicit values. In a clause, a function is preceded by an [operator], which in turn is preceded by a [field]. A function performs a calculation on either specific Confluence data or the function's content in parentheses, such that only true results are retrieved by the function and then again by the clause in which the function is used.

This document also covers the reserved <a href="https://confluence.atlassian.com/display/JIRA/Advanced+Searching+Functions#AdvancedSearchingFunctions-characters" class="external-link">characters</a> and <a href="https://confluence.atlassian.com/display/JIRA/Advanced+Searching+Functions#AdvancedSearchingFunctions-words" class="external-link">words</a> that Confluence uses.

**On this page:**

-   [Functions Reference]
-   [List of Functions]
-   [Reserved Characters]
-   [Reserved Words]

**Related topics:**

-   [Advanced Searching using CQL][search for content in Confluence]
-   [Performing text searches using CQL]
-   [CQL Field Reference]

## List of Functions

-   [currentUser()]
-   [endOfDay()]
-   [endOfMonth()]
-   [endOfWeek()]
-   [endOfYear()]
-   [startOfDay()]
-   [endOfMonth()][1]
-   [startOfWeek()]
-   [startOfYear()]
-   [favouriteSpaces()]
-   [recentlyViewedContent()]
-   [recentlyViewedSpaces()]

#### currentUser()

Perform searches based on the currently logged-in user.

Note that this function can only be used by logged-in users. Anonymous users cannot use this function.

###### Syntax

``` syntaxhighlighter-pre
currentUser()
```

###### Supported Fields

-   [Creator]
-   [Contributor]
-   [Mention]
-   [Watcher]
-   [Favourite]

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
</tr>
</tbody>
</table>

###### Examples

-   Find content that was created by me

    ``` syntaxhighlighter-pre
    creator = currentUser()
    ```

-   Find content that mentions me but wasn't created by me

    ``` syntaxhighlighter-pre
    mention = currentUser() and creator != currentUser()
    ```

[^top of functions] | [^^top of topic]

#### endOfDay()

Perform searches based on the end of the current day. See also [endOfWeek], [endOfMonth] and [endOfYear]; and [startOfDay], [startOfWeek], [startOfMonth] and [startOfYear].

###### Syntax

``` syntaxhighlighter-pre
endOfDay()
```

or

``` syntaxhighlighter-pre
endOfDay("inc")
```

where `inc` is an optional increment of `(+/-)nn(y|M|w|d|h|m)`

-   If the plus/minus `(+/-)` sign is omitted, plus is assumed.
-   nn: number; y: year, M: month; w: week; d: day; h: hour; m: minute.

###### Supported Fields

-   [Created]
-   [Lastmodified]

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
</tr>
</tbody>
</table>

###### Examples

-   Find content created since the end of yesterday

    ``` syntaxhighlighter-pre
    created > endOfDay("-1d")
    ```

[^top of functions] | [^^top of topic]

#### endOfMonth()

Perform searches based on the end of the current month. See also [endOfDay], [endOfWeek] and [endOfYear]; and [startOfDay], [startOfWeek], [startOfMonth] and [startOfYear].

###### Syntax

``` syntaxhighlighter-pre
endOfMonth()
```

or

``` syntaxhighlighter-pre
endOfMonth("inc")
```

where `inc` is an optional increment of `(+/-)nn(y|M|w|d|h|m)`

-   If the plus/minus `(+/-)` sign is omitted, plus is assumed.
-   nn: number; y: year, M: month; w: week; d: day; h: hour; m: minute.

###### Supported Fields

-   [Created]
-   [Lastmodified]

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
</tr>
</tbody>
</table>

###### Examples

-   Find content modified before the end of the month

    ``` syntaxhighlighter-pre
    lastmodified < endOfMonth()
    ```

[^top of functions] | [^^top of topic]

#### endOfWeek()

Perform searches based on the end of the current week. See also [endOfDay], [endOfMonth] and [endOfYear]; and [startOfDay], [startOfWeek], [startOfMonth] and [startOfYear].

###### Syntax

``` syntaxhighlighter-pre
endOfWeek()
```

or

``` syntaxhighlighter-pre
endOfWeek("inc")
```

where `inc` is an optional increment of `(+/-)nn(y|M|w|d|h|m)`

-   If the plus/minus `(+/-)` sign is omitted, plus is assumed.
-   nn: number; y: year, M: month; w: week; d: day; h: hour; m: minute.

###### Supported Fields

-   [Created]
-   [Lastmodified]

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
</tr>
</tbody>
</table>

###### Examples

-   Find content created after the end of last week

    ``` syntaxhighlighter-pre
    created > endOfWeek("-1w")
    ```

[^top of functions] | [^^top of topic]

#### endOfYear()

Perform searches based on the end of the current year. See also [startOfDay], [startOfWeek] and [startOfMonth]; and [endOfDay], [endOfWeek], [endOfMonth] and [endOfYear].

``` syntaxhighlighter-pre
endOfYear()
```

or

``` syntaxhighlighter-pre
endOfYear("inc")
```

where `inc` is an optional increment of `(+/-)nn(y|M|w|d|h|m)`

-   If the plus/minus `(+/-)` sign is omitted, plus is assumed.
-   nn: number; y: year, M: month; w: week; d: day; h: hour; m: minute.

###### Supported Fields

-   [Created]
-   [Lastmodified]

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
</tr>
</tbody>
</table>

###### Examples

-   Find content created by the end of this year:

    ``` syntaxhighlighter-pre
    created < endOfYear()
    ```

[^top of functions] | [^^top of topic]

#### startOfDay()

Perform searches based on the start of the current day. See also [endOfWeek], [endOfMonth] and [endOfYear]; and [startOfDay], [startOfWeek], [startOfMonth] and [startOfYear].

###### Syntax

``` syntaxhighlighter-pre
startOfDay()
```

or

``` syntaxhighlighter-pre
startOfDay("inc")
```

where `inc` is an optional increment of `(+/-)nn(y|M|w|d|h|m)`

-   If the plus/minus `(+/-)` sign is omitted, plus is assumed.
-   nn: number; y: year, M: month; w: week; d: day; h: hour; m: minute.

###### Supported Fields

-   [Created]
-   [Lastmodified]

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
</tr>
</tbody>
</table>

###### Examples

-   Find content created since the start of today

    ``` syntaxhighlighter-pre
    created > startOfDay()
    ```

-   Find content created in the last 7 days

    ``` syntaxhighlighter-pre
    created > startOfDay("-7d")
    ```

[^top of functions] | [^^top of topic]

#### endOfMonth()

Perform searches based on the start of the current month. See also [endOfDay], [endOfWeek] and [endOfYear]; and [startOfDay], [startOfWeek], [startOfMonth] and [startOfYear].

###### Syntax

``` syntaxhighlighter-pre
startOfMonth()
```

or

``` syntaxhighlighter-pre
startOfMonth("inc")
```

where `inc` is an optional increment of `(+/-)nn(y|M|w|d|h|m)`

-   If the plus/minus `(+/-)` sign is omitted, plus is assumed.
-   nn: number; y: year, M: month; w: week; d: day; h: hour; m: minute.

###### Supported Fields

-   [Created]
-   [Lastmodified]

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
</tr>
</tbody>
</table>

###### Examples

-   Find content created since the start of the month

    ``` syntaxhighlighter-pre
    created >= startOfMonth()
    ```

[^top of functions] | [^^top of topic]

#### startOfWeek()

Perform searches based on the start of the current week. See also [endOfDay], [endOfMonth] and [endOfYear]; and [startOfDay], [startOfWeek], [startOfMonth] and [startOfYear].

###### Syntax

``` syntaxhighlighter-pre
startOfWeek()
```

or

``` syntaxhighlighter-pre
startOfWeek("inc")
```

where `inc` is an optional increment of `(+/-)nn(y|M|w|d|h|m)`

-   If the plus/minus `(+/-)` sign is omitted, plus is assumed.
-   nn: number; y: year, M: month; w: week; d: day; h: hour; m: minute.

###### Supported Fields

-   [Created]
-   [Lastmodified]

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
</tr>
</tbody>
</table>

###### Examples

-   Find content created since the start of the week

    ``` syntaxhighlighter-pre
    created >= startOfWeek()
    ```

[^top of functions] | [^^top of topic]

#### startOfYear()

Perform searches based on the start of the current year. See also [startOfDay], [startOfWeek] and [startOfMonth]; and [endOfDay], [endOfWeek], [endOfMonth] and [endOfYear].

``` syntaxhighlighter-pre
startOfYear()
```

or

``` syntaxhighlighter-pre
startOfYear("inc")
```

where `inc` is an optional increment of `(+/-)nn(y|M|w|d|h|m)`

-   If the plus/minus `(+/-)` sign is omitted, plus is assumed.
-   nn: number; y: year, M: month; w: week; d: day; h: hour; m: minute.

###### Supported Fields

-   [Created]
-   [Lastmodified]

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
</tr>
</tbody>
</table>

###### Examples

-   Find content created this year

    ``` syntaxhighlighter-pre
    created >= startOfYear()
    ```

#### favouriteSpaces()

Returns a list of space keys, corresponding to the favourite spaces of the logged in user. 

###### Syntax

``` syntaxhighlighter-pre
favouriteSpaces()
```

###### Supported Fields

-   [Space]

###### Supported Operators

###### 

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
</tr>
</tbody>
</table>

###### Examples

-   Find content which exists in the favourite spaces of the logged in user 

``` syntaxhighlighter-pre
space IN favouriteSpaces()
```

-   Find content which exists in the favourite spaces of the logged in user as well as other listed spaces

``` syntaxhighlighter-pre
space IN (favouriteSpaces(), 'FS', 'TS')
```

###### Available from version

5.9

###### [^top of functions] | [^^top of topic]

#### recentlyViewedContent()

Returns a list of IDs of recently viewed content for the logged in user.

###### Syntax

``` syntaxhighlighter-pre
recentlyViewedContent(limit, offset)
```

###### Supported Fields

-   ancestor
-   content
-   id
-   parent

Supported Operators

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
</tr>
</tbody>
</table>

Examples

-   Find contents with limit to recent 10 history

``` syntaxhighlighter-pre
id in recentlyViewedContent(10)
```

-   Find contents with limit to recent 10 history, but skip first 20

``` syntaxhighlighter-pre
id in recentlyViewedContent(10, 20)
```

###### Available from version

5.9

[^top of functions] | [^^top of topic]

#### recentlyViewedSpaces()

Returns a list of key of spaces recently viewed by the logged in user.

###### Syntax

``` syntaxhighlighter-pre
recentlyViewedSpaces(limit)
```

###### Supported Fields

-   space

Supported Operators

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
</tr>
</tbody>
</table>

Examples

-   Find spaces with limit to recent 10 history

``` syntaxhighlighter-pre
 space in recentlyViewedSpaces(10)
```

###### Available from version

5.9

[^top of functions] | [^^top of topic]

 

 

## Reserved Characters

CQL has a list of reserved characters :

-   **space** (`" "`)
-   `"+"`
-   `"."`
-   `","`
-   `";"`
-   `"?"`
-   `"|"`
-   `"*"`
-   `"/"`
-   `"%"`
-   `"^"`
-   `"$"`
-   `"#"`
-   `"@"`
-   `"["`
-   `"]"`

 

If you wish to use these characters in queries, you need to:

-   surround them with quote-marks (you can use either single quote-marks (`'`) or double quote-marks (`"`));
    **and,** if you are searching a text field and the character is on the list of <a href="https://confluence.atlassian.com/display/JIRA/Performing+Text+Searches#PerformingTextSearches-escaping" class="external-link">reserved characters for Text Searches</a>,
-   precede them with two backslashes.

## Reserved Words

CQL has a list of reserved words. These words need to be surrounded by quote-marks if you wish to use them in queries:

"after", "and", "as", "avg", "before", "begin", "by","commit", "contains"**,** "count", "distinct", "else", "empty", "end", "explain", "from", "having", "if", "in", "inner", "insert", "into", "is", "isnull", "left", "like", "limit", "max", "min", "not", "null", "or", "order", "outer", "right", "select", "sum", "then", "was", "where", "update"

 

 

  [search for content in Confluence]: /confcloud/advanced-searching-using-cql-39985862.html
  [operator]: https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-operators
  [field]: https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-fields
  [Functions Reference]: #CQLFunctionReference-functionfunctionsFunctionsReference
  [List of Functions]: #CQLFunctionReference-ListofFunctions
  [Reserved Characters]: #CQLFunctionReference-charactersReservedCharacters
  [Reserved Words]: #CQLFunctionReference-wordsReservedWords
  [Performing text searches using CQL]: /confcloud/performing-text-searches-using-cql-39985876.html
  [CQL Field Reference]: /confcloud/cql-field-reference-39985865.html
  [currentUser()]: #CQLFunctionReference-currentUser()
  [endOfDay()]: #CQLFunctionReference-endOfDayendOfDay()
  [endOfMonth()]: #CQLFunctionReference-endOfMonthendOfMonth()
  [endOfWeek()]: #CQLFunctionReference-endOfWeekendOfWeek()
  [endOfYear()]: #CQLFunctionReference-endOfYearendOfYear()
  [startOfDay()]: #CQLFunctionReference-startOfDaystartOfDay()
  [1]: #CQLFunctionReference-startOfMonthendOfMonth()
  [startOfWeek()]: #CQLFunctionReference-startOfWeekstartOfWeek()
  [startOfYear()]: #CQLFunctionReference-startOfYearstartOfYear()
  [favouriteSpaces()]: #CQLFunctionReference-favouriteSpacesfavouriteSpaces()
  [recentlyViewedContent()]: #CQLFunctionReference-recentlyViewedContentrecentlyViewedContent()
  [recentlyViewedSpaces()]: #CQLFunctionReference-recentlyViewedSpacesrecentlyViewedSpaces()
  [Creator]: https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-creatorcreatorCreator
  [Contributor]: https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-contributorcontributorContributor
  [Mention]: https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-mentionMentionMention
  [Watcher]: https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-Watcher
  [Favourite]: https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-favouriteFavouriteFavourite,favorite
  [^top of functions]: #CQLFunctionReference-functions
  [^^top of topic]: #CQLFunctionReference-top
  [endOfWeek]: #CQLFunctionReference-endOfWeek
  [endOfMonth]: #CQLFunctionReference-endOfMonth
  [endOfYear]: #CQLFunctionReference-endOfYear
  [startOfDay]: #CQLFunctionReference-startOfDay
  [startOfWeek]: #CQLFunctionReference-startOfWeek
  [startOfMonth]: #CQLFunctionReference-startOfMonth
  [startOfYear]: #CQLFunctionReference-startOfYear
  [Created]: https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-createdCreatedCreated
  [Lastmodified]: https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-lastmodifiedLastModifiedLastModified
  [endOfDay]: #CQLFunctionReference-endOfDay
  [Space]: https://developer.atlassian.com/display/CONFDEV/CQL+Field+Reference#CQLFieldReference-spaceSpaceSpace

