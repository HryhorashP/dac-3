---
title: Adding Macro Content to a Page 39986418
aliases:
    - /confcloud/adding-macro-content-to-a-page-39986418.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39986418
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39986418
confluence_id: 39986418
platform:
product:
category:
subcategory:
---
# Confluence Connect : Adding macro content to a page

## Graphic elements required

To add your macro to the insert menu, select macro dialog, and macro placeholder, you'll need the following:

-   A brand icon to be used by the <a href="https://developer.atlassian.com/static/connect/docs/latest/modules/common/web-item.html" class="external-link">web items</a> in the insert menu and the macro browser. For best results, provide a square image that's at least 80x80 pixels.
-   Optionally, you can provide an <a href="https://developer.atlassian.com/static/connect/docs/latest/modules/confluence/dynamic-content-macro.html#imagePlaceholder" class="external-link">image placeholder</a> to represent what the content will look like in the editor (if you don't provide one, your brand icon is shown)

![]

 

## How do users add your macro?

Users can insert your macro in the editor by either:

1.  Using the insert ![][1] button in the toolbar
2.  Pasting a link or url (auto convert)

## UI components

*Your macro listed in the insert menu*

<img src="/confcloud/attachments/39986418/39986410.png" class="image-center" />

*
*

*The macro browser: You'll need a short text description for your macro*

![][2]

 

*The macro placeholder in the editor*

*![][3]*

 

*The macro editor*

![][4]

 

*The macro properties panel*

*![][5]*

 

*Auto converting a URL*

*![][6]*

*
*

* ![][7]*

*
*

*![][3]*

 

## Recommendations 

-   Write a concise paragraph that describes your add-on.
-   If possible, use a flat visual style for your icon.
-   Use a placeholder image to show a sample preview of your add-on. It helps users understand what the content will look like when they switch from editing the page to viewing it.

*
*

*
*

  []: /confcloud/attachments/39986418/39986408.png
  [1]: /confcloud/attachments/39986418/39986407.png
  [2]: /confcloud/attachments/39986418/39986411.png
  [3]: /confcloud/attachments/39986418/39986412.png
  [4]: /confcloud/attachments/39986418/39986413.png
  [5]: /confcloud/attachments/39986418/39986414.png
  [6]: /confcloud/attachments/39986418/39986415.png
  [7]: /confcloud/attachments/39986418/39986416.png

