---
title: Displaying in the Full Search 39988790
aliases:
    - /confcloud/displaying-in-the-full-search-39988790.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39988790
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39988790
confluence_id: 39988790
platform:
product:
category:
subcategory:
---
# Confluence Connect : Displaying in the full search

## Graphic elements required

An icon of your content type with the following specs:

-   16x16px in size, optimised for retina display
-   Transparent background
-   Line weight of 1px for icon graphics
-   Colour \#707070 only

If no icon is provided, we'll use the default object attachment icon ![].

## How do I optimise this?

The full search provides additional functionality to help users find the right content. You can use the [search extension pattern] to help users when they're trying to find custom content.

## How does this interaction work?

Users can search from the search pages using a variety of keywords

-   Results show in the full screen as a list
-   User can apply additional filters to help narrow down the results

## UI components in the flow

![][1]

 

## Recommendation

-   -   Do not use coloured icons or excessive branding.
    -   Use only simple graphics.

  []: /confcloud/attachments/39988790/44063048.png
  [search extension pattern]: /confcloud/search-extensions-39985059.html
  [1]: /confcloud/attachments/39988790/39988784.png

