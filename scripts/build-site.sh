#!/bin/bash

set -x

function setpyg {
    echo '+++ Setting PygmentsCodeFences = $1 in config.toml'
    sed -i "s/PygmentsCodeFences\s*=\s*false/PygmentsCodeFences = $1/" config.toml
}

while getopts "p" opt; do
    case "$opt" in
        p) setpyg true
           ;;
    esac
done
shift $((OPTIND-1))

./scripts/gen-version-stamp.sh

for i in themes/*; do
    echo "====== Building theme $i ====="
    pushd $i

    if [ -f package.json ]; then
        echo "++ Building Node/NPM packages"
        npm install
        npm run build
    fi

    popd
    echo "++ Done"
done

echo "====== Building Connect Docs ====="
pushd scripts/connect
npm install
npm run build
popd
echo "++ Done"

echo "===== Building site with theme adg3 ====="

echo "+++ Hugo config is..."
hugo config

. scripts/enter-virtualenv.sh

echo "+++ Running Hugo..."
hugo --theme=adg3 $@

# FIXME: We want a blank base-url for proxy passthru/aliases to work,
# but Hugo throws an error when baseurl is empty, although it builds
# fine. This means we need to discard errors for now until we are able
# to set the baseurl after migration.
exit 0
