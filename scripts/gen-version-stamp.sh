#!/bin/bash

set -ex

h=`git log -1 --pretty=format:"%h"`
t=`date -u '+%Y%m%d%H%M%S'`
version="$h-$t"

echo "+++ Setting release version to: $version"
echo $version > release.txt
