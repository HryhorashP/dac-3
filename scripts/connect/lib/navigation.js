var yaml = require('js-yaml');
var fs = require('fs');
var _ = require('lodash');
var helper = require('./helper');

const NAVIGATION_PATH = '../../data/navigation.yml';

class Navigation {
  constructor() {
    this.data = yaml.safeLoad(fs.readFileSync(NAVIGATION_PATH)); 
  }

  getProducts() {
    return Object.keys(this.data.prodnames);
  }

  addItemToSubcategory(item, dest) {
    if (!dest || !dest.product || !dest.category || !dest.subcategory) {
      throw new Error('Missing product, category or subcategory.');
    }

    if (!item || !item.title || !item.url) {
      throw new Error('Item must contain title and url.');
    }

    // find the subcategory
    let thisSubcategory = this.getSubcategory(dest.product, dest.category, dest.subcategory);
  
    // check if item already exists
    let itemIndex = helper.findIndexOf(thisSubcategory.items, 'title', item.title);
    if (itemIndex > -1) {
      // overwrite existing one
      thisSubcategory.items[itemIndex] = item;
      console.log(`Overwritten ${item.title} to ${dest.product}.${dest.category}.${dest.subcategory}`);
    } else {
      // add item
      thisSubcategory.items.push(item);
      console.log(`Added ${item.title} to ${dest.product}.${dest.category}.${dest.subcategory}`);
    }
  }

  getProductNavigation(product) {
    var productIndex = helper.findIndexOf(this.data.Navigation, 'name', product);
    if (!this.data.prodnames[product] || productIndex < 0) {
      throw new Error(`${product} is a not a valid product.`);
    }
    return this.data.Navigation[productIndex];
  }

  getCategory(product, category) {
    var thisProduct = this.getProductNavigation(product);
    var categoryIndex = helper.findIndexOf(thisProduct.categories, 'name', category);
    if (categoryIndex < 0) {
      throw new Error(`${category} is a not a valid category in ${product}.`);
    }
    return thisProduct.categories[categoryIndex];
  }

  getSubcategory(product, category, subcategory) {
    var thisCategory = this.getCategory(product, category);
    var subcategoryIndex = helper.findIndexOf(thisCategory.subcategories, 'name', subcategory);
    if (subcategoryIndex < 0) {
      throw new Error(`${subcategory} is a not a valid subcategory of ${category} in ${product}.`);
    }
    return thisCategory.subcategories[subcategoryIndex];
  }

  write() {
    fs.writeFile(NAVIGATION_PATH, yaml.safeDump(this.data), (err) => {
      if (err) {
        console.log(err);
      }
    })
  }
}

module.exports = new Navigation();

